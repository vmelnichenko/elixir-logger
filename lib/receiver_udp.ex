defmodule ReceiverUDP do
  require Logger

  def child_spec(opts) do
    %{
      id:        __MODULE__,
      start:    {__MODULE__, :start, opts},
      type:     :worker,
      restart:  :permanent,
      shutdown: 500000
    }
  end

  def start(port) do
      case :gen_udp.open(port, [:binary, active: false]) do
        {:ok, socket} ->
          Logger.info("UDP opened.")
          accept_connection(socket)
          :gen_udp.close(socket)
        {:error, reason} -> Logger.error("Could not listen UDP: #{reason}")
      end
  end

  defp accept_connection(socket) do
    case :gen_udp.recv(socket, 0) do
      {:ok, {_,_,packet}} ->
        spawn_link fn ->
          case String.split_at( packet, 7 ) do
            {"loginit", ident} ->
              Logger.info( "Try to Open file #{ident}" )

              case File.open( ( String.trim(ident) <> ".log"), [:append, :binary] ) do
                {:ok   , file       } -> Logger.info( "Open file #{ident}" ); serve(socket,file)
                {:error, file_reason} -> Logger.info( "Cannot open file #{ident} #{file_reason}" )
              end
            {_,_} -> :ok
          end
        end
        accept_connection(socket)
      {:error, :closed} -> :ok
      {:error, reason } -> Logger.info("UDP Socket terminating: #{inspect reason}")
    end
  end

  ### move to base class
  defp serve(socket,file) do
    case :gen_tcp.recv(socket, 0, 100000000) do
      {:ok   , data    } -> IO.binwrite file, data; serve(socket,file)
      {:error, :timeout} -> :timer.sleep(20); serve(socket,file)
      {:error, :closed } -> :ok
      {:error, reason  } -> Logger.info("Socket terminating: #{inspect reason}")
    end
  end

end
