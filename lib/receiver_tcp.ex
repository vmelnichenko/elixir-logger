defmodule ReceiverTCP do
  require Logger

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start, opts},
      type: :worker,
      restart: :permanent,
      shutdown: 500
    }
  end

  def start(port) do
    case :gen_tcp.listen(port, [:binary, active: false, reuseaddr: true, keepalive: true, buffer: 65536]) do
      {:ok, socket} ->
        Logger.info("TCP opened.")
        accept_connection(socket)
        :gen_tcp.close(socket)
      {:error, reason} -> Logger.error("Could not listen TCP: #{reason}")
    end
  end

  defp accept_connection(socket) do
    case :gen_tcp.accept(socket) do
      {:ok, client} ->
        pid = spawn_link fn -> serve(client) end
        Logger.info("NEW CLIENT: #{inspect pid}")
        accept_connection(socket)
      {:error, :closed} -> :ok
    end
  end

  defp serve(socket) do
    case :gen_tcp.recv(socket, 0) do
      {:ok, data} ->
        case String.split_at( data, 7 ) do
          {"loginit", ident} ->
            case File.open( ( String.trim(ident) <> ".log"), [:append, :binary] ) do
              {:ok   , file       } -> Logger.info( "Open file #{ident}" ); serve(socket,file); File.close( file )
              {:error, file_reason} -> Logger.info( "Cannot open file #{ident} #{file_reason}" )
            end
          {_,_} -> serve(socket) ### will ignore any other messages
        end
      {:error, :timeout} -> :timer.sleep(20); serve(socket)
      {:error, :closed } -> :ok
      {:error, reason  } -> Logger.info("Socket terminating: #{inspect reason}")
    end
  end

  defp serve(socket,file) do
    case :gen_tcp.recv(socket, 0) do
      {:ok   , data    } -> IO.binwrite file, data; serve(socket,file)
      {:error, :timeout} -> :timer.sleep(20)      ; serve(socket,file)
      {:error, :closed } -> :ok
      {:error, reason  } -> Logger.info("Socket terminating: #{inspect reason}")
    end
  end

end
