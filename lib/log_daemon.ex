defmodule LogDaemon do
#  use Application
  require ReceiverTCP
  require ReceiverUDP
  require Logger
#  import Supervisor.Spec

  @moduledoc """
  Documentation for LogDaemon.
  """

  @doc """
  Hello world.

  ## Examples

      iex> LogDaemon.hello()
      :world

  """

  def start(_type, _args) do
    {:ok, _} = Supervisor.start_link([
      {ReceiverTCP, [8080]},
    ], strategy: :one_for_one)
  end
end
